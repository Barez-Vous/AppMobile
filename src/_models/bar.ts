export interface Bar {
  id: number;
  name: string;
  latitude: string;
  longitude: string;
  houseNumber: string;
  street: string;
  postCode: boolean;
  city: boolean;
  country: string;
  email: string;
  phoneNumber: string;
  facebookPage: string;
  websiteUrl: string;
  openingTimes: [string];
  happyHours: [string];
  note:number;
}
