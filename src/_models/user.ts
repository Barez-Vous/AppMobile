export interface User {
  id: number;
  username: string;
  email: string;
  email_confirmation: string;
  password: string;
  password_confirmation: string;
  current_password: string;
  useRealName: boolean;
  isEnabled: boolean;
  firstName: string;
  lastName: string;
  lastLogin: string;
  dateCreated: string;
  roles: [string];
  picture: string;
}
