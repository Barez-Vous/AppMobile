import { Pipe, PipeTransform } from '@angular/core';
import { Bar } from '../_models';

@Pipe({
  name: 'openingTimes'
})
export class OpeningTimesPipe implements PipeTransform {
  transform (input : any, bar: Bar): any {
    let currentDate = new Date;
    let day = currentDate.getDay() - 1;
    if (day == -1)
      day = 6;
    let previousDay = day - 1;
    let returnValue;
    let currentTime = currentDate.toTimeString();
    // Vérifie si le bar a des horaires pour le jour ou la veille (bar fermé après minuit)
    if (!bar.openingTimes || !bar.openingTimes[day]) {
      if (!bar.openingTimes[previousDay])
        returnValue = "";
      else {
        // Si le bar n'a pas d'horaires pour la journée en cours mais pour la veille
        // Des horaires a 00:00:00 pour l'ouverture et la fermeture signifie que le bar est fermé ce jour là
        let openingTime = bar.openingTimes[previousDay]['opening_time'];
        let closingTime = bar.openingTimes[previousDay]['closing_time'];
        if ((openingTime == "00:00:00") && (closingTime == "00:00:00"))
          returnValue = "Fermé";
        else if (closingTime <= currentTime)
          returnValue = "Fermé";
        else if ((currentTime <= closingTime) && (closingTime <= openingTime))
          returnValue = "Ouvert jusqu'à " + closingTime;
      }
      return returnValue;
    }
    let openingTime = bar.openingTimes[day]['opening_time'];
    let closingTime = bar.openingTimes[day]['closing_time'];
    // Si le bar a des horaires la veille, on vérifie si on est dans les horaires d'ouvertures de la veille
    if (bar.openingTimes[previousDay]) {
      if ((currentTime <= bar.openingTimes[previousDay]['closing_time']) 
      && (currentTime <= bar.openingTimes[previousDay]['opening_time']) 
      && (bar.openingTimes[previousDay]['closing_time'] <= bar.openingTimes[previousDay]['opening_time']))
        returnValue = "Ouvert jusqu'à" + bar.openingTimes[previousDay]['closing_time'];
    }
    // Sinon on vérifie les horaires de la journée
    if ((openingTime == "00:00:00") && (closingTime == "00:00:00"))
      returnValue = "Fermé";
    else if (currentTime <= openingTime)
      returnValue = "Ouvre à " + openingTime;
    else if ((openingTime <= currentTime) && (currentTime <= closingTime)) {
      if (bar.happyHours) {
        if (currentTime <= bar.happyHours[day]['starting_time']) {
          returnValue = "Happy Hours jusqu'à " + bar.happyHours[day]['ending_time']
        } else if (currentTime <= bar.happyHours[day]['starting_time']) {
          returnValue = "Happy Hours à " + bar.happyHours[day]['starting_time']
        } else {
          returnValue = "Ouvert jusqu'à" + closingTime;
        }
      } else {
        returnValue = "Ouvert jusqu'à" + closingTime;
      }
    }
    return returnValue;
  }
}