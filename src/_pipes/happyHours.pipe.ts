import { Pipe, PipeTransform } from '@angular/core';
import { Bar } from '../_models';

@Pipe({
  name: 'happyHours'
})
export class HappyHoursPipe implements PipeTransform {
  transform (input : any, bar: Bar): any {
      let currentDate = new Date;
      let day = currentDate.getDay() - 1;
      if (day == -1)
        day = 6;
      let previousDay = day - 1;
      let returnValue;
      let currentTime = currentDate.toTimeString();
      // Vérifie si le bar a des horaires pour le jour ou la veille (bar fermé après minuit)
      if (!bar.happyHours || !bar.happyHours[day]) {
        if (!bar.happyHours[previousDay])
          returnValue = "";
        else {
          // Si le bar n'a pas d'horaires pour la journée en cours mais pour la veille
          // Des horaires a 00:00:00 pour l'ouverture et la fermeture signifie que le bar est fermé ce jour là
          let openingTime = bar.happyHours[previousDay]['opening_time'];
          let closingTime = bar.happyHours[previousDay]['closing_time'];
          if ((openingTime == "00:00:00") && (closingTime == "00:00:00"))
            returnValue = "";
          else if (closingTime <= currentTime)
            returnValue = "";
          else if (currentTime <= closingTime)
          returnValue = "Happy Hours";
        }
        return returnValue;
      }
      let openingTime = bar.happyHours[day]['opening_time'];
      let closingTime = bar.happyHours[day]['closing_time'];
      // Si le bar a des horaires la veille, on vérifie si on est dans les horaires d'ouvertures de la veille
      if (bar.happyHours[previousDay]) {
        if ((currentTime <= bar.happyHours[previousDay]['closing_time']) && (currentTime <= bar.happyHours[previousDay]['opening_time']))
          returnValue = "Happy Hours";
      }
      // Sinon on vérifie les horaires de la journée
      if ((openingTime == "00:00:00") && (closingTime == "00:00:00"))
        returnValue = "";
      else if (currentTime <= openingTime)
        returnValue = "";
      else if ((openingTime <= currentTime) && (currentTime <= closingTime))
      returnValue = "Happy Hours";
      
      return returnValue;
  }
}