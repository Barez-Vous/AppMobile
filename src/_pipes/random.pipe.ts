import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'random'
})
export class RandomPlaceholderPipe implements PipeTransform {
  transform (input : any, min: number = 0, max: number = 1): any {
    if (min > max) {
      max = min;
      min = 0;
    }
    let randomNumber = Math.floor(Math.random() * (max - min) + min)
    return "assets/imgs/placeholder/placeholder"+randomNumber+'.jpg';
  }
}