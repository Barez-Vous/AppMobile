import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Bar } from '../_models/index';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class BarService {

  private baseUrl: string = 'http://spielmannromain.fr/BarezVous/web';

  constructor(private http:Http, public authenticationService: AuthenticationService) { }


  private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    let headers = new Headers({ "Content-Type": "application/json" });
    headers.append('Accept', 'application/json');
      let currentUser = this.authenticationService.getCurrentUser();
      if (currentUser && currentUser.token) {
          headers.append( 'X-Access-Token', currentUser.token );
      }
    return headers;
  }

  getAllAsJSON(): any {
    let bars$ = this.http
      .get(`${this.baseUrl}/api/bars`, {headers: this.getHeaders()})
      .map(this.extractData)
      return bars$;
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  getByRange(lat:number, long:number, range:string): Observable<Bar[]>{
    let bars$ = this.http
      .post(`${this.baseUrl}/api/bar/byRange`, JSON.stringify({ latitude: lat, longitude: long, range: range }) , {headers: this.getHeaders()})
      .map(this.mapBars);
      return bars$;
  }

  get(id: number) : Observable<Bar>{
    let bar$ = this.http
      .get(`${this.baseUrl}/api/bar/${id}`, {headers: this.getHeaders()})
      .map(this.mapUser);
      return bar$;
  }

  getComments(id: number) : Observable<Bar>{
    let commentaires$ = this.http
      .get(`${this.baseUrl}/api/bar/commentaire/${id}`, {headers: this.getHeaders()})
      .map((response: Response) => response.json());
      return commentaires$;
  }

  addComments(commentaire: any): Observable<Response>{
    return this.http
      .post(`${this.baseUrl}/api/secured/commentaire/create`, commentaire, {headers: this.getHeaders()})
      .map((response: Response) => response.json());
  }

  delete(id: number) {
    return this.http
    .delete(`${this.baseUrl}/api/secured/bar/${id}`, {headers: this.getHeaders()})
    .map((response: Response) => response.json());
  }

  mapBars(response:Response): Bar[]{
     // The response of the API has a results
     // property with the actual results
     return response.json()
  }

  save(bar: Bar) {
    if (bar.id) {
      let mappedBar = {
        id: bar.id,
        name: bar.name,
        latitude: bar.latitude,
        longitude: bar.longitude,
        houseNumber: bar.houseNumber,
        street: bar.street,
        postCode: bar.postCode,
        city: bar.city,
        country: bar.country,
        email: bar.email,
        phoneNumber: bar.phoneNumber,
        facebookPage: bar.facebookPage,
        websiteUrl: bar.websiteUrl
      }
      return this.http
      .put(`${this.baseUrl}/api/secured/bar/${bar.id}`, mappedBar, {headers: this.getHeaders()})
      .map((response: Response) => response.json());
    } else {
      let mappedBar = {
        name: bar.name,
        latitude: bar.latitude,
        longitude: bar.longitude,
        houseNumber: bar.houseNumber,
        street: bar.street,
        postCode: bar.postCode,
        city: bar.city,
        country: bar.country,
        email: bar.email,
        phoneNumber: bar.phoneNumber,
        facebookPage: bar.facebookPage,
        websiteUrl: bar.websiteUrl
      }
      return this.http
      .post(`${this.baseUrl}/api/secured/bar/create`, mappedBar, {headers: this.getHeaders()})
      .map((response: Response) => response.json());
    }
  }

  saveHoraires(bar: Bar) {
    let mappedBar = {
      id: bar.id,
      openingTimes : bar.openingTimes
    }
    return this.http
    .post(`${this.baseUrl}/api/secured/bar/horaires/${bar.id}`, mappedBar, {headers: this.getHeaders()})
    .map((response: Response) => response.json());
  }

  saveHappyHours(bar: Bar) {
    let mappedBar = {
      id: bar.id,
      happyHours : bar.happyHours
    }
    return this.http
    .post(`${this.baseUrl}/api/secured/bar/happy_hours/${bar.id}`, mappedBar, {headers: this.getHeaders()})
    .map((response: Response) => response.json());
  }

  mapUser(response:Response): Bar{
     return response.json();
  }

}
