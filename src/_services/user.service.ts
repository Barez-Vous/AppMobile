import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { User } from '../_models/index';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class UserService {

  private baseUrl: string = 'http://spielmannromain.fr/BarezVous/web';

  constructor(private http:Http, public authenticationService: AuthenticationService) { }


  private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    let headers = new Headers({ "Content-Type": "application/json" });
    headers.append('Accept', 'application/json');
      let currentUser = this.authenticationService.getCurrentUser();
      if (currentUser && currentUser.token) {
          headers.append( 'X-Access-Token', currentUser.token );
      }
    return headers;
  }

  getAllAsJSON(): any {
    let users$ = this.http
      .get(`${this.baseUrl}/api/secured/users`, {headers: this.getHeaders()})
      .map(this.extractData)
      return users$;
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  getAll(): Observable<User[]>{
    let users$ = this.http
      .get(`${this.baseUrl}/api/secured/users`, {headers: this.getHeaders()})
      .map(this.mapUsers);
      return users$;
  }

  get(id: number) : Observable<User>{
    let user$ = this.http
      .get(`${this.baseUrl}/api/user/${id}`, {headers: this.getHeaders()})
      .map(this.mapUser);
      return user$;
  }

  create(user: User) {
    return this.http
    .post(`${this.baseUrl}/register`, user, {headers: this.getHeaders()})
    .map((response: Response) => response.json());
  }

  save(user: User) {
    let mappedUser = {
      id: user.id,
      email: user.email,
      username: user.username,
      roles: user.roles,
      customFields : {
        firstName : user.firstName,
        lastName : user.lastName,
      }
    }
    return this.http
    .post(`${this.baseUrl}/profil/${user.id}`, mappedUser, {headers: this.getHeaders()})
    .map((response: Response) => response.json());
  }

  update(user: User) {
    let mappedUser = {
      email: user.email,
      email_confirmation: user.email_confirmation,
      password: user.password,
      password_confirmation: user.password_confirmation,
      username: user.username,
      current_password: user.current_password,
      customFields : {
        firstName : user.firstName,
        lastName : user.lastName,
      }
    }
    return this.http
    .post(`${this.baseUrl}/profil/-1`, mappedUser, {headers: this.getHeaders()})
    .map((response: Response) => response.json());
  }

  delete(id: number) {
      return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
  }

  mapUsers(response:Response): User[]{
     // The response of the API has a results
     // property with the actual results
     return response.json()
  }

  mapUser(response:Response): User{
     return response.json();
  }
  private jwt() {
      // create authorization header with jwt token
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (currentUser && currentUser.token) {
          let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
          return new RequestOptions({ headers: headers });
      }
  }

}
