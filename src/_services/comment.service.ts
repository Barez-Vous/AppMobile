import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Comment } from '../_models/index';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class CommentService {

  private baseUrl: string = 'http://spielmannromain.fr/BarezVous/web';

  constructor(private http:Http, public authenticationService: AuthenticationService) { }


  private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    let headers = new Headers({ "Content-Type": "application/json" });
    headers.append('Accept', 'application/json');
      let currentUser = this.authenticationService.getCurrentUser();
      if (currentUser && currentUser.token) {
          headers.append( 'X-Access-Token', currentUser.token );
      }
    return headers;
  }

  getAllAsJSON(): any {
    let comments$ = this.http
      .get(`${this.baseUrl}/api/secured/comments`, {headers: this.getHeaders()})
      .map(this.extractData)
      return comments$;
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  getAll(): Observable<Comment[]>{
    let comments$ = this.http
      .get(`${this.baseUrl}/api/secured/comments`, {headers: this.getHeaders()})
      .map(this.mapComments);
      return comments$;
  }

  get(id: number) : Observable<Comment>{
    let comment$ = this.http
      .get(`${this.baseUrl}/api/comment/${id}`, {headers: this.getHeaders()})
      .map(this.mapComment);
      return comment$;
  }

  create(comment: Comment) {
    return this.http
      .post(`${this.baseUrl}/api/secured/commentaire/create`, comment, {headers: this.getHeaders()})
      .map((response: Response) => response.json());
  }

  mapComments(response:Response): Comment[]{
     // The response of the API has a results
     // property with the actual results
     return response.json()
  }

  mapComment(response:Response): Comment{
     return response.json();
  }

}
