import { Injectable } from '@angular/core';
import { Connectivity } from './connectivity';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@Injectable()
export class GoogleMaps {
 
  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  currentMarker: any;
  apiKey: string = "AIzaSyDb5w-i6Wg7jGbGdXMdn1Ow6I0OuqQBq2w";
 
  constructor(public connectivityService: Connectivity,
  	          public geolocation: Geolocation) {
  }
 
  init(mapElement: any): Promise<any> {
    this.mapElement = mapElement;
    this.pleaseConnect = false;
    return this.loadGoogleMaps();
  }
 
  loadGoogleMaps(): Promise<any> {
    return new Promise((resolve) => {
      if(typeof google == "undefined" || typeof google.maps == "undefined"){
        if(this.connectivityService.isOnline()){
          window['mapInit'] = () => {
            this.initMap().then((map) => {
              resolve(map);
            });
          }
          let script = document.createElement("script");
          script.id = "googleMaps";
          if(this.apiKey){
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';      
          }
          document.body.appendChild(script); 
        }
      }
      else {
        if(this.connectivityService.isOnline()){
          this.initMap();
        }
      }
      this.addConnectivityListeners();
    });
  }
 
  initMap(): Promise<any> {
    this.mapInitialised = true;
    return new Promise((resolve) => {
      this.geolocation.getCurrentPosition().then((position) => {
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        //let latLng = new google.maps.LatLng(-31.563910, 147.154312);
 
        let mapOptions = {
          center: latLng,
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        resolve(this.map);
      });
    });
  }
 
  addConnectivityListeners(): void {
    this.connectivityService.watchOnline().subscribe(() => {
      setTimeout(() => {
        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        }
        else {
          if(!this.mapInitialised){
            this.initMap();
          }
        }
      }, 2000);
    });
  }
}