import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';


// Pages
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { BarListPage } from '../pages/bar-list/bar-list';

// Services
import { AuthenticationService, UserService, BarService } from '../_services/index';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
// Gmaps plugin
import { AgmCoreModule } from '@agm/core';
import { GoogleMaps } from '../providers/google-maps';
import { Connectivity } from '../providers/connectivity';

// Native plugins
import { LaunchNavigator } from '@ionic-native/launch-navigator'; 
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppAvailability } from '@ionic-native/app-availability';
import { Network } from '@ionic-native/network';


// Internationalization module
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RandomPlaceholderPipe } from '../_pipes/random.pipe';
import { OpeningTimesPipe } from '../_pipes/openingTimes.pipe';
import { HappyHoursPipe } from '../_pipes/happyHours.pipe';

// Fonction permettant de chercher les fichiers de langue dans ./assets/i18n plutôt que ./i18n/
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    RegisterPage,
    LoginPage,
    BarListPage,
    RandomPlaceholderPipe,
    OpeningTimesPipe,
    HappyHoursPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDb5w-i6Wg7jGbGdXMdn1Ow6I0OuqQBq2w',
      libraries : ["places"]
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    Ng2SearchPipeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    RegisterPage,
    LoginPage,
    BarListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    Network,
    LaunchNavigator,
    InAppBrowser,
    AppAvailability,
    AuthenticationService,
    UserService,
    BarService,
    GoogleMaps,
    Connectivity
  ]
})
export class AppModule {}
