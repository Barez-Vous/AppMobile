import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService } from '../../_services/index';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  model: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService,
    public authenticationService: AuthenticationService,
    public events: Events
  ) {
    translate.setDefaultLang('fr');
  }

  login() {
      this.authenticationService.login(this.model.email, this.model.password)
          .subscribe(
              data => {
                  this.events.publish('user:login');
              });
  }
  // googleLogin() {
  //   this.platform.ready()
  //     .then(this.googleLoginCall)
  //     .then(success => {
  //     }, (error) => {
  //       alert(error);
  //     });
  // }
  // googleLoginCall(): Promise<any> {
  //   return new Promise(function(resolve, reject) {
  //     const clientId = "897812875695-4mg1qdbntudlkcftgb36hbtouqfdlltc.apps.googleusercontent.com";
  //     const url = `https://accounts.google.com/o/oauth2/auth?client_id=${clientId}` +
  //               "&redirect_uri=http://localhost" +
  //               "&scope=https://www.googleapis.com/auth/plus.login" +
  //               "&response_type=token";
  //     const browserRef = window.cordova.InAppBrowser.open(
  //           url,
  //           "_blank",
  //           "location=no, clearsessioncache=yes, clearcache=yes"
  //     );
  //     let responseParams : string;
  //     let parsedResponse : Object = {};
  //     browserRef.addEventListener("loadstart", (evt) => {
  //         if ((evt.url).indexOf("http://localhost") === 0) {
  //           browserRef.removeEventListener("exit", (evt) => {});
  //           browserRef.close();
  //           responseParams = ((evt.url).split("#")[1]).split("&");
  //           for (var i = 0; i < responseParams.length; i++) {
  //             parsedResponse[responseParams[i].split("=")[0]] = responseParams[i].split("=")[1];
  //           }
  //           if (parsedResponse["access_token"] !== undefined &&
  //               parsedResponse["access_token"] !== null) {
  //             resolve(parsedResponse);
  //           } else {
  //             reject("Problème d’authentification avec Google");
  //           }
  //         }
  //     });
  //     browserRef.addEventListener("exit", function(evt) {
  //       reject("Une erreur est survenue lors de la tentative de connexion à Google");
  //     });
  //   });
  //}
}
