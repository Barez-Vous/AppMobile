import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BarListPage } from './bar-list';

@NgModule({
  declarations: [
    BarListPage,
  ],
  imports: [
    IonicPageModule.forChild(BarListPage),
  ],
})
export class BarListPageModule {}
