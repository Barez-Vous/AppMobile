import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { Bar } from './../../_models/bar';
import { BarService } from './../../_services/index';

import { RandomPlaceholderPipe } from '../../_pipes/random.pipe';
import { OpeningTimesPipe } from '../../_pipes/openingTimes.pipe';

@IonicPage()
@Component({
  selector: 'page-bar-list',
  templateUrl: 'bar-list.html',
})
export class BarListPage {
  bars: Bar[];
  barAround: Bar[];
  searchString: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
  	public geolocation: Geolocation,
    private barService: BarService,
    public modalCtrl: ModalController) {
      this.barService
      .getAllAsJSON()
      .subscribe(bars => {
        this.bars = bars;
      });
      events.subscribe('bar:search', (searchString) => {
        this.searchString = searchString;
      });
  }

  getBars(){
    this.geolocation.getCurrentPosition().then(position => {
      //On applique a la recherche une portée en km, ici 10 km autour de l'utilisateur
      this.barService.getByRange(position.coords.latitude, position.coords.longitude, "10")
          .subscribe(
          data => {
              this.barAround = data;
          },
          error => {
              console.log("Erreur numéro : " + error.status);
          });

  	    },
        error => {
            console.log("Erreur numéro : " + error.status);
        });
		}
  ionViewDidLoad() {
    this.getBars();
  }

  public openDetails(barId) {
    let barModal = this.modalCtrl.create("BarPage", {barId : barId});
    barModal.present();
  }
}
