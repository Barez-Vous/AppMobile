import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core'
import { LoginRegisterPage } from './login-register';

@NgModule({
  declarations: [
    LoginRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginRegisterPage),
    TranslateModule.forChild()
  ],
})
export class LoginRegisterPageModule {}
