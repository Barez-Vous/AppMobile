import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, Events } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';

import { User } from '../../_models/index';
import { AuthenticationService } from '../../_services/index';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the LoginRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-register',
  templateUrl: 'login-register.html',
})
export class LoginRegisterPage {
    tab1Root = LoginPage;
    tab2Root = RegisterPage;
    currentUser : User;

    constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private authenticationService: AuthenticationService,
      public modalCtrl: ModalController,
      public viewCtrl: ViewController,
      public translate: TranslateService,
      public events: Events) {
        translate.setDefaultLang('fr');
        events.subscribe('user:login', () => {
          this.closeModal();
        });
        events.subscribe('user:register', () => {
          this.closeModal();
        });
    }

    ionViewDidLoad() {
      this.currentUser = this.authenticationService.getCurrentUser();
    }

    closeModal() {
      this.viewCtrl.dismiss();
    }

}
