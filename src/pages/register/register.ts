import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Http } from '@angular/http';

import { UserService } from '../../_services/index';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage {
  model: any = {};
  loading = false;
  RegisterForm: FormGroup;
  http;
  submitAttempt: boolean = false;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
    public translate: TranslateService,
    public formBuilder: FormBuilder,
    private userService: UserService,
    public events: Events,
    http: Http
  ) {
    translate.setDefaultLang('fr');
    this.http = http;
    this.RegisterForm = formBuilder.group({
        username: ['', Validators.compose([
          Validators.maxLength(255),
          Validators.pattern('[a-zA-Z0-9]*'),
          this.checkUsername.bind(this)])],
        firstName: ['', Validators.compose([
          Validators.maxLength(80),
          Validators.pattern('[a-zA-Z ]*')])],
        lastName: ['', Validators.compose([
          Validators.maxLength(255),
          Validators.pattern('[a-zA-Z ]*')])],
        email: ['', Validators.compose([
          Validators.required,
          Validators.maxLength(255),
          this.checkEmail.bind(this)
          ])],
        password: ['', Validators.required],
        passwordConfirm: ['', Validators.compose([
               Validators.required,
               this.isEqualPassword.bind(this)
           ])]
    });
  }

  register() {
    this.loading = true;
    this.userService.create(this.model)
      .subscribe(
        data => {
          this.loading = false;
          this.events.publish('user:login');
        },
        error => {
          this.loading = false;
        });
  }

  isEqualPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.RegisterForm) {
      return {"passwordsNotMatch": true};

    }
    if (control.value !== this.RegisterForm.controls['password'].value) {
      return {"passwordsNotMatch": true};
    }
  }

  checkUsername(control: FormControl): {[s: string]: boolean} {
    var link = 'http://localhost:8001/BarezVous/backend/public/checkUsername';
    var data = JSON.stringify({
      username: control.value
    });
    return this.http.post(link, data)
      .subscribe(data => {
        if (data['_body'].response == true)
          return {"usernameExists": true};
        else
          return null;
      }, error => {
        return {"usernameExists": true};
      });
  }

  checkEmail(control: FormControl): {[s: string]: boolean} {
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
      return {"emailExist": true};
    }
    var link = 'http://localhost:8001/BarezVous/backend/public/checkEmail';
    var data = JSON.stringify({
      email: control.value
    });
    this.http.post(link, data)
      .subscribe(data => {
        if (data._body['response'] == 'true')
          return {"emailExist": true};
      }, error => {
        return {"emailExist": true};
      });
    return {"emailExist": true}
  }
}
