import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BarPage } from './bar';

@NgModule({
  declarations: [
    BarPage,
  ],
  imports: [
    IonicPageModule.forChild(BarPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class BarPageModule {}
