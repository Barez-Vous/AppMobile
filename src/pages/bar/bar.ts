import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController, ModalController } from 'ionic-angular';
import { AuthenticationService } from './../../_services/authentication.service';
import { User } from './../../_models/user';
import { Bar } from './../../_models/bar';
import { BarService } from './../../_services/index';

import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-bar',
  templateUrl: 'bar.html',
})
export class BarPage {
  require: any;
  barId: number;
  noteComment: number;
  model: any = {};
  bar: Bar;
  barComments: {};
  currentUser: User;
  //Map pour faire le lien entre la valeur de la bdd et l'affichage
  mapJour = new Map([
    ["mon", "Lundi"],
    ["tue", "Mardi"],
    ["wed", "Mercredi"],
    ["thu", "Jeudi"],
    ["fri", "Vendredi"],
    ["sat", "Samedi"],
    ["sun", "Dimanche"]
  ]);

  showHoraires = false;
  showHappyHours = false;
  horairesIcon= "ios-add-circle-outline";
  happyHoursIcon= "ios-add-circle-outline";

  constructor(
    public platform: Platform,
    private barService: BarService,
    private authenticationService: AuthenticationService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private launchNavigator: LaunchNavigator,
    private iab: InAppBrowser,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) { }

  ionViewDidLoad() {
    this.currentUser = this.authenticationService.getCurrentUser();
    //Recuperation de la position de l'utilisateur
    //Quand on a le callback de la postion on recupere l'id dans le lien
    this.barId = this.navParams.get('barId');
    //On recupere les infos du bar par son id
    this.barService.get(this.barId)
      .subscribe(
      data => {
        this.bar = data;
        //On recupere les commentaires liés a ce bar
        this.barService.getComments(this.barId)
          .subscribe(
          data => {
            this.barComments = data;
          },
          error => {
            console.log("Erreur numéro : " + error.status);
          });
      },
      error => {
        console.log("Erreur numéro : " + error.status);
      });
  };
  //Conversion d'une string en number
  ConvertStringToNumber(value) {
    return parseFloat(value)
  }

  //Fonction effectuée sur l'appui du bouton d'ajout de commentaire
  postReview() {
    if (this.model.commentaire == "") {
      console.log("Commentaire vide");
      return;
    }
    //On lie le commentaire au bar
    this.model.id_bar = this.bar.id;
    this.model.note = this.noteComment;
    //Envoie du commentaire a l'API
    this.barService.addComments(this.model).subscribe(
      data => {
        this.model = {};
        this.ionViewDidLoad();
      },
      error => {
        console.log("Erreur numéro : " + error.status);
      });

  }

  public openWebsite() {
    this.iab.create(this.bar.websiteUrl, '_blank');
  }

  findScheme(): any{
    if(this.platform.is('ios')) {
      return 'facebook';
    } else if (this.platform.is('android')) {
      return 'com.facebook.android';
    } else {
      return false;
    }
  }

  public openFacebook() {
    if (this.findScheme()) {
      window.open('fb://' + this.bar.facebookPage, '_system');
    } else {
      this.iab.create(this.bar.facebookPage, '_blank');
    }
  }

  toggleHoraires() {
    this.showHoraires = !this.showHoraires;
    this.showHoraires ? this.horairesIcon = 'ios-remove-circle-outline' : this.horairesIcon = "ios-add-circle-outline" ;
  }

  toggleHappyHours() {
    this.showHappyHours = !this.showHappyHours;
    this.showHappyHours ? this.happyHoursIcon = 'ios-remove-circle-outline' : this.happyHoursIcon = "ios-add-circle-outline" ;
  }
  //Fonction effectué sur le changement de note sur un commentaire
  changeStar(note: number) {
    this.noteComment = note;
  }

  public openMap(longitude, latitude) {
    this.launchNavigator.navigate(latitude + ', ' + longitude)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
}
