import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController, Platform } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { Bar, User } from './../../_models';
import { BarService, AuthenticationService } from './../../_services';

import { GoogleMaps } from '../../providers/google-maps';

import * as MarkerClusterer from 'node-js-marker-clusterer';
import 'rxjs/add/operator/map';

declare var google;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  location = {};
  bars : Bar[];
  currentUser : User;

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markerCluster :  any;

  constructor(
  	public navCtrl: NavController,
  	public geolocation: Geolocation,
    private barService: BarService,
    public platform: Platform, 
    public maps: GoogleMaps,
    public modalCtrl: ModalController,
    public authenticationService: AuthenticationService) {}

  ionViewDidLoad(){
    this.currentUser = this.authenticationService.getCurrentUser();
    this.geolocation.getCurrentPosition().then(
      position => {
        this.platform.ready().then(() => {
          this.maps.init(this.mapElement.nativeElement).then((map) => {
            this.addMarker(this.maps.map, position.coords.latitude, position.coords.longitude, 0);
            this.getBars(position.coords.latitude, position.coords.longitude, 10);
            let that = this;
            google.maps.event.addListener(map, 'click', function(event) {
              that.getBars(event.latLng.lat(), event.latLng.lng(), 10);
            })
          });
        });
      },
      error => {
        console.log("Erreur numéro : " + error.status);
      }
    );
  }

  getBars(latitude, longitude, range){
    //On applique a la recherche une portée en km, ici 10 km autour de l'utilisateur
    this.barService.getByRange(latitude, longitude, "10")
      .subscribe(
        data => {
          this.clearMap();
          this.bars = data;
          this.addCluster(this.maps.map, this.bars);
        },
        error => {
          console.log("Erreur numéro : " + error.status);
        }
      );
  }
  //Icone Bar'ez-Vous! pour la carte
  private iconMapBV = {
    url: 'assets/imgs/LogoBarezVous.png',
    scaledSize: {
        height: 50,
        width: 50
    }
  };

  addMarker(map, lat, long, barId){
    let latLng = new google.maps.LatLng(lat, long);
    let marker;
    if (barId > 0) {
      marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        position: latLng,
        icon: this.iconMapBV
      });  
   
      google.maps.event.addListener(marker, 'click', () => {
        let barModal = this.modalCtrl.create("BarPage", {barId : barId});
        barModal.present();
      });
    } else {
      marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        position: latLng,
      });  
    }
    //this.addInfoWindow(map, marker, content);
    return marker;
  }

  addInfoWindow(map, marker, content){
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(map, marker);
    });
  }

  addCluster(map, bars){
    let markers = bars.map((bar) => {
        return this.addMarker(map, bar.latitude, bar.longitude, bar.id);
    });
    this.markerCluster = new MarkerClusterer(map, markers, {imagePath: 'assets/imgs/m'});
  }

  clearMap() {
    if (this.markerCluster) {
      this.markerCluster.clearMarkers();
    }
  }

  addBar() {
    let addBarModal = this.modalCtrl.create("AddBarPage");
    addBarModal.present();
  }
}
