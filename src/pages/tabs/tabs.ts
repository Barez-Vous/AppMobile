import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, Events, Tabs } from 'ionic-angular';

import { HomePage } from '../home/home';
import { BarListPage } from '../bar-list/bar-list';

import { User } from '../../_models/index';
import { AuthenticationService } from '../../_services/index';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild("tabs")
  tabs: Tabs;
  tab1Root = HomePage;
  tab2Root = BarListPage;
  currentUser : User;
  public toggled: boolean = false;

  placeholder = "Rechercher un bar";
  cancelButtonText = "×"
  constructor(
    private authenticationService: AuthenticationService,
    public events: Events,
    private nav: NavController,
    public modalCtrl: ModalController) {
    events.subscribe('user:login', () => {
      this.ionViewDidLoad();
    });

  }

  public toggle() {
     this.toggled = this.toggled ? false : true;
  }

  ionViewDidLoad() {
    this.toggled = false;
    this.currentUser = this.authenticationService.getCurrentUser();
  }

  public searchThis(ev) {
    if (!this.tabs._tabs[1].isSelected) {
      this.tabs.select(1);
    }
    let searchString = ev.target.value;
    this.events.publish('bar:search', searchString);
  }

  public cancelSearch(event) {
    this.toggle();
  }

  public login() {
    let loginModal = this.modalCtrl.create("LoginRegisterPage");
    loginModal.present();
  }

  public logout() {
    this.authenticationService.logout();
    this.ionViewDidLoad();
  }
}
