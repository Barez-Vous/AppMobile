import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, ModalController, Platform, ViewController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { Bar, User } from './../../_models';
import { BarService, AuthenticationService } from './../../_services';
import { GoogleMaps } from '../../providers/google-maps';

declare var google;
@IonicPage()
@Component({
  selector: 'page-add-bar',
  templateUrl: 'add-bar.html',
})
export class AddBarPage {
  currentUser : User;

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
  addressElement: HTMLInputElement = null;
  map: any;
  markers: any;
  autocomplete = {
    input: ''
  };
  GoogleAutocomplete: any;
  GooglePlaces: any;
  geocoder: any
  autocompleteItems: any;
  loaded = false;

  constructor(
    public navCtrl: NavController,
    public geolocation: Geolocation,
    private barService: BarService,
    public zone: NgZone,
    public platform: Platform, 
    public maps: GoogleMaps,
    public modalCtrl: ModalController,
    public authenticationService: AuthenticationService,
    public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    this.currentUser = this.authenticationService.getCurrentUser();
    this.geolocation.getCurrentPosition().then(
      position => {
        this.platform.ready().then(() => {
          this.maps.init(this.mapElement.nativeElement).then((map) => {
            console.log('ICI');
            this.map = map;
            this.geocoder = new google.maps.Geocoder;
            let elem = document.createElement("div")
            this.GooglePlaces = new google.maps.places.PlacesService(elem);
            this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
            this.autocompleteItems = [];
            this.markers = [];
            this.loaded = true;
          });
        });
      },
      error => {
        console.log("Erreur numéro : " + error.status);
      }
    );
  }

  updateSearchResults(){
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }

  selectSearchResult(item){
    this.clearMarkers();
    this.autocompleteItems = [];
  
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      if(status === 'OK' && results[0]){
        let position = {
            lat: results[0].geometry.location.lat,
            lng: results[0].geometry.location.lng
        };
        let marker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: this.map,
        });
        this.markers.push(marker);
        this.map.setCenter(results[0].geometry.location);
      }
    })
  }
  
  clearMarkers(){
    for (var i = 0; i < this.markers.length; i++) {
      console.log(this.markers[i])
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
}
