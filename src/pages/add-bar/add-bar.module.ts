import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBarPage } from './add-bar';

@NgModule({
  declarations: [
    AddBarPage,
  ],
  imports: [
    IonicPageModule.forChild(AddBarPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AddBarPageModule {}
